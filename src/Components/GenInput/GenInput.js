import React, {Component} from 'react';
import './GenInput.css';

class GenInput extends Component {
    render() {
        return (
            <label>
                <input type="text" onChange={this.props.onChange} value={this.props.value}/>
            </label>
        );
    }
}

export default GenInput;