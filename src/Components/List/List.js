import React, {Component} from 'react';
import ListComponents from "./ListComponent/ListComponents";



class List extends Component {

    render() {
        return (
            <div className="ListOfMovies">
                {this.props.movies.map(movie => (
                    <ListComponents movies={movie} delete={this.props.delete} add={this.props.changeInput}/>
                ))}
            </div>
        );
    }
}

export default List;