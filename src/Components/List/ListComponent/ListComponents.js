import React, {Component} from 'react';
import Button from "../../Button/Button";
import './ListComponents.css';

class ListComponents extends Component {
    shouldComponentUpdate(nextProps) {
        return nextProps.label !== this.props.movies.label || nextProps.id !== this.props.movies.id
    }
    render() {
        return (
            <div key={this.props.movies.id} className='list'>
                <input type="text"  className="input-list" value={this.props.movies.label} onChange={e => this.props.changeInput(this.props.movies.id, e.target.value)}/>
                <Button type="Delete" click={() => this.props.delete(this.props.movies.id)}/>
            </div>
        );
    }
}

export default ListComponents;