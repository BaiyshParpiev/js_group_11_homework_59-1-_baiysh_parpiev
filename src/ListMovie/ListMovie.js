import React, {Component} from 'react';
import GenInput from "../Components/GenInput/GenInput";
import List from "../Components/List/List";
import Button from "../Components/Button/Button";

class ListMovie extends Component {
    state = {
        savedList: localStorage.getItem('movies'),
        genInput: '',
        movies: []
    }

    componentDidMount () {
        const getMovies = JSON.parse(this.state.savedList);
        if(this.state.savedList){
            this.setState(prevState => ({
                ...prevState,
                movies: [...this.state.movies, ...getMovies],
            }));
        }
    }

    valueFromGenInput = e => {
        this.setState({genInput: e.target.value})
    }

    add = e => {
        e.preventDefault();
        this.setState(prevState => ({
            ...prevState,
            movies:[...this.state.movies, {id: Math.random() * 100, label: this.state.genInput}],
        }));

        this.setState(prevState => ({
            ...prevState,
            genInput: '',
        }))
    }

    delete = id => {
        this.setState(prev => ({
            ...prev,
            movies: this.state.movies.filter(m => m.id !== id)
                // .map(m =>{
                // return {...m, number: m.number - 1}
            // })
        }));
    }

    onChangeInput = (id, e) => {
        this.setState(prev => ({
            ...prev,
            movies: this.state.movies.map(movie => {
                if(movie.id === id){
                    return {...movie, label: e}
                }
                return movie;
            })
        }));
    }

    componentDidUpdate() {
        localStorage.setItem('movies', JSON.stringify(this.state.movies));
    };

    render() {
        return (
            <>
                <div className="container">
                    <GenInput onChange={e => this.valueFromGenInput(e)} value={this.state.genInput}/>
                    <Button type="Add" click={this.add}/>
                </div>
                <List changeInput={this.onChangeInput} movies={this.state.movies} delete={this.delete}/>
                <Button type="Save" click={this.save}/>
            </>
        );
    }
}

export default ListMovie;